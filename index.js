import * as Path from "path";
import { readFile } from "fs/promises";
import { SourceMapGenerator, SourceMapConsumer } from "source-map";
import { transformAsync } from "@babel/core";
import solid from "babel-preset-solid";

export default {
  name: "solid-js",
  setup(build) {
    build.onLoad({ filter: /\.jsx$/ }, async (args) => {
      const source = await readFile(args.path, { encoding: "utf8" });

      const filename = Path.basename(args.path);

      const { code, map } = await transformAsync(source, {
        presets: [[solid, {}]],
        filename,
        sourceMaps: true,
      });

      const dataurl = Buffer.from(
        SourceMapGenerator.fromSourceMap(
          await new SourceMapConsumer(map)
        ).toString()
      ).toString("base64");

      return {
        contents:
          code +
          "\n" +
          `//# sourceMappingURL=data:application/json;base64,${dataurl}`,
        loader: "js",
      };
    });
  },
};
