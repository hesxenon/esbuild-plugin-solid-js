# Overview
This is a simple plugin that transforms jsx files according to `babel-preset-solid` with the addition of truly inlining sourcemaps.

# Usage
```typescript
import solid from "esbuild-plugin-solid-js"

Esbuild.build({
  plugins: [solid]
})
```

# A note about Typescript configuration
In order for solid and sourcemaps to work with typescript you should set the following configurations in your `tsconfig.json`

```json
{
	"jsx": "preserve",
	"jsxImportSource": "solid-js",
	"inlineSources": true,
	"inlineSourceMap": true,
	"inlineSources": true
}
```
Remember that this plugin _ONLY_ transpiles `jsx` files, it does _NOT_ perform any form of typescript transpilation
